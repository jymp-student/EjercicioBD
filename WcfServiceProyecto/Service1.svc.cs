﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceProyecto
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        private Model1Container DB = new Model1Container();

        public List<USUARIO> EditarUsuario(string Usu)
        {
            return EditarUsuario(Usu);
        }

        public List<USUARIO> MostrarUsuario()
        {
            return DB.USUARIOSet.ToList();
        }

        public USUARIO MostrarUs(string Id)
        {
            int numId = Convert.ToInt16(Id);
            return DB.USUARIOSet.Where(x => x.Id == numId).First();
        }

        public USUARIO EliminarUsuario(string Id)
        {
            return DB.USUARIOSet.Remove(DB.USUARIOSet.Find(Id.Equals(Id)));
        }

        // Persona

        public List<PERSONA> EditarPersona(string Per)
        {
            return EditarPersona(Per);
        }

        public List<PERSONA> MostrarPersona()
        {
            return DB.PERSONASet.ToList();
        }

        public PERSONA MostrarPers(string Id)
        {
            int numId = Convert.ToInt16(Id);
            return DB.PERSONASet.Where(x => x.Id == numId).First();
        }

        public PERSONA EliminarPersona(string Id)
        {
            return DB.PERSONASet.Remove(DB.PERSONASet.Find(Id.Equals(Id)));
        }
        /*
        public List<USUARIO> VerUsuario()
        {
            return DB.USUARIOSet.ToList();
        }

        public USUARIO VerUs(int id)
        {
            int numId = Convert.ToInt16(id);
            return DB.USUARIOSet.Where(x => x.Id == numId).First();
            //   return personas.Find(x => x.Id == id);
        }

        public bool ActualizarUsu(USUARIO dto)
        {
            return true;
        }
        */

        /*
            public List<USUARIO> MostrarUsuario()
            {
                return DB.USUARIOSet.ToList();
            }

            public USUARIO MostrarUs(string Id)
            {
                int numId = Convert.ToInt16(Id);
                return DB.USUARIOSet.Where(p => p.Id == numId).First();
            }

            public List<PERSONA> MostrarPersona()
            {
                return DB.PERSONASet.ToList();
            }

            public PERSONA MostrarPer(string Id)
            {
                int nuId = Convert.ToInt16(Id);
                return DB.PERSONASet.Where(p => p.Id == nuId).First();
            }

            public List<USUARIO> EliminarUsuario()
            {
                return DB.USUARIOSet.ToList();
            }

            public USUARIO EliminarUs(string Id)
            {
                int nId = Convert.ToInt16(Id);
                return DB.USUARIOSet.Where(p => p.Id == nId).First();
            }
            */

    }
}
