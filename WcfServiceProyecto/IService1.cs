﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceProyecto
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "EditarUsuario", ResponseFormat = WebMessageFormat.Json)]
        List<USUARIO> EditarUsuario(string Usu);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarUsuario", ResponseFormat = WebMessageFormat.Json)]
        List<USUARIO> MostrarUsuario();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarUsuario/{Id}", ResponseFormat = WebMessageFormat.Json)]
        USUARIO MostrarUs(string Id);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "EliminarUsuario?Id=(Id)", ResponseFormat = WebMessageFormat.Json)]
        USUARIO EliminarUsuario(string Id);


        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "EditarPersona", ResponseFormat = WebMessageFormat.Json)]
        List<PERSONA> EditarPersona(string Per);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarPersona", ResponseFormat = WebMessageFormat.Json)]
        List<PERSONA> MostrarPersona();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarPersona/{Id}", ResponseFormat = WebMessageFormat.Json)]
        PERSONA MostrarPers(string Id);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "EliminarPersona?Id=(Id)", ResponseFormat = WebMessageFormat.Json)]
        PERSONA EliminarPersona(string Id);

        /*
        [WebGet(UriTemplate = "/VerUsuarios",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<USUARIO> VerUsuario();

        [WebGet(UriTemplate = "/VerUsuarios?id={id}",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        USUARIO VerUs(int id);

        [WebInvoke(UriTemplate = "ActualizarUsuario",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            Method = "POST")]

        bool ActualizarUsu(USUARIO dto);
        */

        /*
        //USUARIO

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarUsuario", ResponseFormat = WebMessageFormat.Json)]
        List<USUARIO> MostrarUsuario();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarUsuario/{Id}", ResponseFormat = WebMessageFormat.Json)]
        USUARIO MostrarUs(string Id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarUsuario", ResponseFormat = WebMessageFormat.Json)]
        List<USUARIO> EliminarUsuario();

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "EliminarUsuario/{Id}", RequestFormat = WebMessageFormat.Json)]
        USUARIO EliminarUs(string Id);

        //PERSONA

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarPersona", ResponseFormat = WebMessageFormat.Json)]
        List<PERSONA> MostrarPersona();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarPersona/{Id}", ResponseFormat = WebMessageFormat.Json)]
        PERSONA MostrarPer(string Id);

         * */
    }
}

